package eu.foxcom.gtphotos.model.functionInterface;

public interface BiConsumer <T1, T2> {
    public void accept(T1 t1, T2 t2);
}
